import { PwsAdtestPage } from './app.po';

describe('pws-adtest App', () => {
  let page: PwsAdtestPage;

  beforeEach(() => {
    page = new PwsAdtestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
